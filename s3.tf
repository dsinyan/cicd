provider "aws" {
  region = "us-east-2"
}

resource "aws_s3_bucket" "my_bucket" {
  bucket = "dehyusbuckk3"
  acl    = "private"

  tags = {
    Name = "ci/cd terraform bucket"
  }
}
