# Terraform + Gitlab
This demo introduces how to use Gitlab CI to managed complex terraform codebases with ease. 

## Folders

_ci: This directory contain `yaml` files for each gitlab stages. 

applications: This directory contains several application specific folders to manage infrastructure for unique applications using terraform

modules: this directory contians s3 modules used in the demo.

